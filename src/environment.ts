export const environment = {
    myEmail: 'saurishphatak@gmail.com',
    gitlabLink: 'https://gitlab.com/saurish_phatak',
    linkedinLink: 'https://www.linkedin.com/in/saurish-phatak/',
    resumeLink: 'https://drive.google.com/file/d/1A24UxuF81AkQ750Nbw-dPdGPom-pKHCX/view?usp=sharing'
}